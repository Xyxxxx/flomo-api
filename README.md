<p align="center">
    <h1 align="center">flomo-api</h1>
    <p align="center">简单抓包了flomo的api</p>
    <p align="center">
        flomo的api都做了签名,需要进行模仿签名
        前端会对发送的内容进行签名(md5)
    </p>
        
    
</p>


## 魔法变量

`dbbc3dd73364b4084c3a69346e0ce2b2`


## 前端代码
```js

static call(t, e, s, a) {
                    const i = "flomo_web"
                      , o = "2.0";
                    let n = this.BASE_URL + e
                      , r = S().unix()
                      , l = {
                        ...s
                    };
                    l.timestamp = r,
                    l.api_key = i,
                    l.app_version = o,
                    l.app_version = this.getPackageVersion(),
                    l.platform = this.getPlatform(),
                    this.isSupportWebp() && (l.webp = "1");
                    let c = JSON.parse(JSON.stringify(l))
                      , d = this._getSign(c);
                    l.sign = d;
                    let h = "";
                    localStorage.getItem("me") && (h = JSON.parse(localStorage.getItem("me")).access_token),
                    $.request({
                        method: t,
                        url: n,
                        params: ["POST", "PUT", "PATCH"].includes(t) ? null : l,
                        data: ["POST", "PUT", "PATCH"].includes(t) ? l : null,
                        headers: {
                            Authorization: "Bearer " + h
                        }
                    }).then((t=>{
                        0 !== t.data.code && (t.data && -10 === t.data.code ? (k.commit("removeMe"),
                        location.reload()) : (console.info(t.data),
                        -1 === n.indexOf("check_password") && v().error(t.data.message))),
                        a(t)
                    }
                    )).catch((t=>{
                        a && a(t)
                    }
                    ))
                }
                static _getSign(t) {
                    const e = "dbbc3dd73364b4084c3a69346e0ce2b2";
                    t = this._ksort(t);
                    let s = "";
                    for (let i in t) {
                        let e = t[i];
                        if ("undefined" !== typeof e && e)
                            if (Array.isArray(e)) {
                                e.sort((function(t, e) {
                                    return t.toString().localeCompare(e.toString())
                                }
                                ));
                                for (let t in e) {
                                    let a = e[t];
                                    s += i + "[]=" + a + "&"
                                }
                            } else
                                s += i + "=" + e + "&"
                    }
                    s = s.substring(0, s.length - 1);
                    let a = M(s + e);
                    return a
                }
                static _ksort(t) {
                    var e = Object.keys(t).sort()
                      , s = {};
                    for (var a in e)
                        s[e[a]] = t[e[a]];
                    return s
                }

```


## 打断点调试

<img src="./static/86e52fb401201ddccef881ecb558079b.png">

最后return的变量a就是sign