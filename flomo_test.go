package flomo

import (
	"fmt"
	"testing"
)

// import (
//
//	"fmt"
//	"testing"
//
// )
const token = "xxxxxxxxxxxxx"

func TestNewWithToken(t *testing.T) {
	NewWithToken(token)
}

func TestNewWithAccount(t *testing.T) {
	email := "xxx@xxxx.com"
	password := "xxxxxxxxxx"

	c := NewWithAccount(email, password)
	fmt.Println(c)
}

func TestGetMono(t *testing.T) {
	c := NewWithToken(token)
	b, err := c.GetAllMono()
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}
	fmt.Println(b)
}

func TestClient_InsertMono(t *testing.T) {
	c := NewWithToken(token)
	err := c.InsertMono("Hello,World")
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}
}
