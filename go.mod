module flomo-api

go 1.19

require (
	github.com/go-resty/resty/v2 v2.7.0
	github.com/tidwall/gjson v1.14.4
)

require (
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/net v0.0.0-20220802222814-0bcc04d9c69b // indirect
)
